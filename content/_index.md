---
title: Photograhpy
linktitle: Home
description: Capturing Emotions Freezing Time
#lastmod: 2023-07-05
featured_image: feature_image.jpg # default: first image in this directory
# featured_image on the home page is used for OpenGraph cards, etc.

# sub-galleries on list pages are sorted by date and weight (descending)
---
